<?php get_header(); ?>
		 <?php include('sidebar2.php'); ?>

   <div id="content">

            <div align="center"><?php if (function_exists('display_my_news_announcement')) { display_my_news_announcement(); }  ?></div>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="post" id="post-<?php the_ID(); ?>"></div>

<div class="post">
		<div class="thedate">Posted <a href="<?php bloginfo('url'); ?>/<?php the_time('Y'); ?>/<?php the_time('m'); ?>/<?php the_time('d'); ?>"><?php the_time('F j, Y'); ?></a><?php the_time(', g:i a'); ?></div>
				<div id="post-<?php the_ID(); ?>">
						<h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>

					<div class="theauthor">By <span class="postauthor"><?php coauthors_posts_links(); ?></span>
					</div>

					<div id="sidebar">
						<div class="feedback">
							<div class="sidebarcomments"><?php comments_popup_link(('Add a comment'), ('Comments (1)'), ('Comments (%)')); ?></div>
							<div class="articletools"><?php if(function_exists('wp_print')) { print_link(); } ?></div>
							<div class="articletools"><?php if(function_exists('wp_email')) { email_link(); } ?></div>
                            <div class="articletools" style="margin-bottom: 8px!important;"><a name="fb_share" type="button_count" share_url="<?php the_permalink(); ?>" href="http://www.facebook.com/sharer.php">Share</a><script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script></div>
                                                        <div class="articletools"><iframe src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&amp;layout=button_count&amp;show_faces=false&amp;width=145&amp;action=like&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:145px; height:21px;" allowTransparency="true"></iframe></div>
                            <div class="articletools">
								<g:plusone size="medium" href="<?php the_permalink(); ?>"></g:plusone>

								<script type="text/javascript">
  									(function() {
   									 var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    									po.src = 'https://apis.google.com/js/plusone.js';
    									var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
 									 })();
								</script></div>
                                                        <div class="articletools">
                                                       	<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
                                                                        <a href="http://twitter.com/share" class="twitter-share-button"
                                                                                data-url="<?php the_permalink(); ?>"
                                                                                data-via="denverpost"
                                                                                data-text="<?php the_title(); ?>:"
                                                                                data-count="horizontal">Tweet</a>
                                                        </div><!--Closes articletools div-->
                                                </div><!--Closes feedback div-->
					</div><!--Closes sidebar div-->

					<div class="storycontent">
						<?php the_content(__('Read more...')); ?>
					</div>
				</div><!-- Closes post-ID div -->
					<div class="postmeta">
                                             <div style="margin-left: 155px; margin-top: 10px;"><strong>Categories:</strong> <?php the_category(', '); ?></div>
					</div>
					<?php trackback_rdf(); ?>
</div> <!-- Closes the post div-->

		<?php comments_template(); // Get wp-comments.php template ?>
		<?php endwhile; else: ?>
       
       <?php endif; ?>
       
			<div class="postnavigation">
				<div class="rightdouble">
					<?php previous_posts_link('Read Newer Entries &raquo;', 0) ?>
				</div>
				<div class="leftdouble">
                    <?php next_posts_link('&laquo; Read Previous Entries', 0); ?>
				</div>

			</div>
         </div><!-- Closes the content div-->

<?php include('footer.php'); ?>