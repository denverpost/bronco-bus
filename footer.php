<div style="clear:both;"></div>

<div class="advertisetext_bot"><a href="http://www.denvernewspaperagency.com/adcenter/" target="_blank">Advertise on The Denver Post</a></div>

<div class="adElement" id="adPosition14" align="center" style="clear:both;">
   <script type="text/javascript" language="JavaScript">yld_mgr.place_ad_here("adPos14");</script>
</div>

<div id="footer">

<!-- Segment Pixel - denverpost entertainment - DO NOT MODIFY -->
<img src="http://ib.adnxs.com/seg?add=199740&t=2" width="1" height="1" />
<!-- End of Segment Pixel -->

 <!--<div id="siteOverview">
   <ul class="siteOverview-majorcolumn">       
      <div class="footerheader"><a href="http://blogs.denverpost.com">Denver Post Blogs</a></div>
         <ul class="siteOverview-column">
            <div class="footerSubhed">News and Business</div>
            <li><a href="http://blogs.denverpost.com/techknowbytes/" target="_blank">TechKnowBytes</a></li>
            <li><a href="http://blogs.denverpost.com/thebalancesheet" target="_blank" title="Colorado business news">The Balance Sheet</a></li>
            <li><a href="http://blogs.denverpost.com/crime" target="_blank" title="Colorado crime news">The Rap Sheet</a></li>
            <li><a href="http://blogs.denverpost.com/health" target="_blank" title="Colorado health news">The Daily Dose</a></li>
            <li><a href="http://blogs.denverpost.com/weather/" target="_blank">Forecast Colorado</a></li>
            <li><a href="http://blogs.denverpost.com/westernslope" target="_blank">On the Western Slope</a></li>
            <li><a href="http://blogs.denverpost.com/coloradoclassroom/" target="_blank">Colorado Classroom</a></li>
            <li><a href="http://blogs.denverpost.com/coldcases/" target="_blank">Cold Cases</a></li>
            <li><a href="http://blogs.denverpost.com/captured/" target="_blank">Plog/Captured</a></li>
         </ul>
         <ul class="siteOverview-column">
            <div class="footerSubhed">Sports</div>
               <li><a href="http://blogs.denverpost.com/broncos/" target="_blank">First-and-Orange</a></li>
               <li><a href="http://blogs.denverpost.com/rockies/" target="_blank">On the Rox</a></li>
               <li><a href="http://blogs.denverpost.com/nuggets/" target="_blank">Nuggets Ink</a></li>
               <li><a href="http://blogs.denverpost.com/avs/" target="_blank">All Things Avs</a></li>
               <li><a href="http://blogs.denverpost.com/rapids/" target="_blank">The Terrace</a></li>
               <li><a href="http://blogs.denverpost.com/colleges/" target="_blank">The Field House</a></li>
               <li><a href="http://blogs.denverpost.com/preps/" target="_blank">Prepped</a></li>
               <li><a href="http://blogs.denverpost.com/sports/" target="_blank">All Things Colorado Sports</a></li>
               <li><a href="http://blogs.denverpost.com/olympics/" target="_blank">All Things Olympics</a></li>
         </ul>
         <ul class="siteOverview-column">
            <div class="footerSubhed">Entertainment</div>
				<li><a href="http://www.heyreverb.com" target="_blank">Reverb</a></li>
               <li><a href="http://blogs.denverpost.com/artmosphere" target="_blank">Artmosphere</a></li>
               <li><a href="http://blogs.denverpost.com/videogames/" target="_blank">Game On</a></li>
               <li><a href="http://blogs.denverpost.com/ostrow/" target="_blank">Ostrow Off the Record</a></li>
               <li><a href="http://blogs.denverpost.com/highernote/" target="_blank">Higher Note</a></li>
               <li><a href="http://blogs.denverpost.com/madmoviegoer/" target="_blank">Diary of a Mad Moviegoer</a></li>
               <li><a href="http://blogs.denverpost.com/runninglines/" target="_blank">Running Lines</a></li>
         </ul>
         
         <ul class="siteOverview-column">
            <div class="footerSubhed">Politics</div>
               <li><a href="http://blogs.denverpost.com/thespot" target="_blank">The Spot</a></li>
         
            <div class="footerSubhed" style="margin-top: 15px;">Outdoors</div>
               <li style="margin-top=15px"><a href="http://blogs.denverpost.com/sports/category/outdoor-extremes/" target="_blank">Outdoor Extremes</a></li>
               <li><a href="http://blogs.denverpost.com/travel/" target="_blank">Free Range</a></li>
            
            <div class="footerSubhed" style="margin-top: 15px;">Opinion</div>
            	<li><a href="http://blogs.denverpost.com/eletters/" target="_blank">eLetters</a></li>
                <li><a href="http://blogs.denverpost.com/carroll/" target="_blank">Vincent Carroll</a></li> 
         </ul>
         
         <ul class="siteOverview-column">
            <div class="footerSubhed">More</div>
               <li><a href="http://blogs.denverpost.com/library/" target="_blank">The Archive</a></li>
               <li><a href="http://blogs.denverpost.com/style" target="_blank">Mile High Style</a></li> 
               <li><a href="http://www.milehighmamas.com/" target="_blank">Mile High Mamas</a></li>
               <li><a href="http://blogs.denverpost.com/ridetherockies" target="_blank">Ride the Rockies</a></li>
         </ul>
         <ul class="siteOverview-column">
            <div class="footerSubhed">Food and Beer</div>
               <li><a href="http://blogs.denverpost.com/food/" target="_blank">Colorado Table</a></li>
               <li><a href="http://blogs.denverpost.com/beer/" target="_blank">First Drafts</a></li>     
         </ul>
       
      </ul>
      
      <ul class="siteOverview-majorcolumn">
         <div class="footerheader"><a href="http://blogs.denverpost.com">Colorado Bloggers</a></div>
         <ul class="siteOverview-column">
            <li><a href="http://joshuabrauer.com/" target="_blank">Joshua Brauer</a></li>
            <li><a href="http://www.artifacting.com/blog/" target="_blank">Artifacting</a></li>
            <li><a href="http://www.intuitive.com/bio.shtml" target="_blank">Ask Dave Taylor</a></li>
            <li><a href="http://www.bikedenver.org/" target="_blank">BikeDenver</a></li>
            <li><a href="http://johncarmichaels.typepad.com/carmichaels_position/" target="_blank">Carmichael's Position</a></li>
            <li><a href="http://causeequalstime.com/" target="_blank">Music: Cause=Time</a></li>
            <li><a href="http://www.realfamiliesrealfun.com/cobrand/blog/" target="_blank">Citizen Mom's Family Journal</a></li>
            <li><a href="http://www.coloradostartups.com/" target="_blank">Colorado Startups</a></li>
            <li><a href="http://coyotegulch.wordpress.com/" target="_blank">Coyote Gulch</a></li>
            <li><a href="http://crazybloggincanuck.blogspot.com/" target="_blank">Crazy Bloggin' Canuck</a></li>
            <li><a href="http://culinary-colorado.blogspot.com/" target="_blank">CulinaryColorado</a></li>
            <li><a href="http://www.impactlab.com/" target="_blank">Impact Lab</a></li>
            <li><a href="http://www.design-klub.com/" target="_blank">Design Klub</a></li>
            <li><a href="http://delicatessen-magazine.blogspot.com/" target="_blank">Delicatessen Magazine</a></li>
            <li><a href="http://www.futuregringo.com/index.php/about/" target="_blank">Future Gringo</a></li>
            <li><a href="http://georgeindenver.wordpress.com/" target="_blank">George in Denver</a></li>
            <li><a href="http://lhaesine.blogspot.com/" target="_blank">Lhaesine Dictionary Project</a></li>
            <li><a href="http://www.thismamacooks.com/" target="_blank">This Mama Cooks!</a></li>
         </ul>
            
         <ul class="siteOverview-column">
            <li><a href="http://www.notesfromagrumpyoldman.blogspot.com/" target="_blank">Notes from a Grumpy Old Man</a></li>
            <li><a href="http://denverprblog.com/" target="_blank">Denver PR Blog</a></li>
            <li><a href="http://scooptoo.com/category/posts" target="_blank">ScoopToo</a></li>
            <li><a href="http://www.milehighsoapbox.com/" target="_blank">Mile High Soapbox</a></li>
            <li><a href="http://seedtospoon.com/" target="_blank">Seed to Spoon</a></li>
            <li><a href="http://godonnybrook.com/home/" target="_blank">The Donnybrook Writing Academy</a></li>
            <li><a href="http://proteinwisdom.com/" target="_blank">Protein Wisdom</a></li>
            <li><a href="http://www.wildsnow.com/" target="_blank">Wild Snow</a></li>
            <li><a href="http://businessword.com/" target="_blank">Business Word</a></li>
            <li><a href="http://aarondelay.com/blog/" target="_blank">A Stroke of Delay</a></li>
            <li><a href="http://seesamwrite.blogspot.com/" target="_blank">See Sam Write</a></li>
            <li><a href="http://rossputin.com/blog/index.php" target="_blank">Rossputin</a></li>
         </ul>
      </ul>
 </div> -->
 
 <p style="clear: both;">&nbsp;</p>
 
 <p class="credits">

All contents Copyright <script language="javascript" type="text/javascript">var d = new Date(); document.write(d.getFullYear());</script> <a href="http://www.denverpost.com/">The Denver Post</a> or other copyright holders.<br>All rights reserved. This material may not be published, broadcast, rewritten or redistributed for any commercial purpose.<br><br><a href="http://www.denverpost.com/ethics">Denver Post Ethics Policy</a> | <a href="http://www.denverpost.com/termsofuse">Terms of Use</a> | <a href="http://www.denverpost.com/privacypolicy">Privacy Policy</a> | <?php wp_loginout(); ?>

<div style="margin: 25px auto;"><a href="http://www.medianewsgroup.com"><img src="http://extras.mnginteractive.com/live/media/mng_logos/mng_footergraphic.jpg" title="MediaNews Group Logo" alt="" /></a></div>

<a href="http://www.denvernewspaperagency.com/adcenter/" title="Advertise">Advertise</a> | <a href="http://www.denverpost.com/archives" title="Archives">Archives</a> | <a href="http://www.denverpost.com/contactus" title="Contact the Denver Post">Contact Us</a> |  <a href="http://www.postnewsmarketplace.com/newsletters/editorial/dpo/prefcenter.asp" title="E-mail Newsletters &amp; Breaking News Alerts">E-mail Alerts</a> | <a href="http://m.denverpost.com" title="Mobile">Mobile</a> | <a href="http://www.denverpost.com/webfeeds" title="Web Feeds">Feeds</a> | <a class="myyahooButton" href="http://us.rd.yahoo.com/my/atm/DenverPost/News%20Breaking/*http://add.my.yahoo.com/rss?url=http%3A//http://feeds.denverpost.com/dp-news-breaking-local">My Yahoo</a> | <a href="http://www.denverpost.com/photostore" title="Photo Store">Buy Denver Post Photos</a> | <a href="http://www.denverpost.com/sitemap" title="Site Map">Site Map</a> | <a href="http://www.denverpost.com/subscribe" title="Subscribe / Customer Care">Home Delivery</a> | <a href="http://www.denverpost.com/viva" title="Viva Colorado">Viva</a><br>

</div>
</div>

<?php wp_footer(); ?>

</body>

</html>