<?php

// widgetize sidebar
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'leftsidebar',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<div class="categoryheader">',
'after_title' => '</div>',
));
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'rightsidebar',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<div class="categoryheader">',
'after_title' => '</div>',
));

// make changeable header

define('HEADER_TEXTCOLOR', '');
define('HEADER_IMAGE', '/wp-content/themes/BroncoBus_Theme/images/white_tile.png'); // %s is theme dir uri
define('HEADER_IMAGE_WIDTH', 495);
define('HEADER_IMAGE_HEIGHT', 78);
define( 'NO_HEADER_TEXT', true );

function DEVS_BlogTheme_admin_header_style() {
?>

<style type="text/css">
#headimg {
	height: <?php echo HEADER_IMAGE_HEIGHT; ?>px;
	width: <?php echo HEADER_IMAGE_WIDTH; ?>px;
}

#headimg h1, #headimg #desc {
	display: none;
}

</style>

<?php
}

function header_style() {
?>
<style type="text/css">

#customheader {
	background: url(<?php header_image(); ?>) no-repeat;
	width: 510px;
	*margin-top: 15px;
	margin-left: 171px;
}

#customheader img {
	margin-left: 0px;
	border-right: 1px solid #999999;
	padding-right: 14px;
	_padding-right: 0px;/*show for IE6 only*/
	_margin-bottom: -3px;/*show for IE6 only*/
}

</style>

<?php
}

add_custom_image_header('header_style', 'DEVS_BlogTheme_admin_header_style');

?>