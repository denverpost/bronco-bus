<?php include('header.php'); ?>
  	 <?php include('sidebar2.php'); ?>
   		<div id="content">
     
   					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
       		<div class="post">
            	
					<div class="thedate">
						Posted <a href="<?php bloginfo('url'); ?>/<?php the_time('Y'); ?>/<?php the_time('m'); ?>/<?php the_time('d'); ?>"><?php the_time('F j, Y'); ?></a><?php the_time(', g:i a'); ?> MT <?php edit_post_link('(Edit)', '', ''); ?>
					</div>
					
                    <h1><?php the_title(); ?></h1>

					<div class="theauthor">      
                                                By <span class="postauthor"><?php
                                                                        $auth = get_the_author();
                                                                        if ($auth == 'freelance') {
                                                                        echo c2c_get_custom('freelance');
                                                                        } else {
                                                                        if(function_exists('coauthors_posts_links')) {
                                                                                                coauthors_posts_links();
                                                                                                } else {
                                                                                                the_author_posts_link();
                                                                                                }
                                                                        }
                                                ?></span>                                 
					</div>
                    
					<div id="post-<?php the_ID(); ?>">
                    </div>
                    
					<div id="sidebar">
						<div class="feedback">
							<div class="sidebarcomments"><a href="<?php comments_link(); ?>"><?php comments_number('Add a comment','Comments (1)','Comments (%)'); ?></a></div>
							<div class="articletools"><?php if(function_exists('wp_print')) { print_link(); } ?></div>
							<div class="articletools"><?php if(function_exists('wp_email')) { email_link(); } ?></div>
                            <div class="articletools" style="margin-bottom: 8px!important;"><a name="fb_share" type="button_count" share_url="<?php the_permalink(); ?>" href="http://www.facebook.com/sharer.php">Share</a><script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script></div>
                            <div class="articletools"><iframe src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&amp;layout=button_count&amp;show_faces=false&amp;width=80&amp;action=like&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe></div>
                            <div class="articletools">
								<g:plusone size="medium" href="<?php the_permalink(); ?>"></g:plusone>

								<script type="text/javascript">
  									(function() {
   									 var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    									po.src = 'https://apis.google.com/js/plusone.js';
    									var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
 									 })();
								</script></div>
                            <div class="articletools"><a href="http://twitter.com/share?url=<?php the_permalink(); ?>" class="twitter-share-button">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>
                                                </div>
                                        
                                                <?php
                                                $posttags = get_the_tags();
                                                if ($posttags) { ?>
                                                <div class="relatedtopicsheader">
                                                      Topics                  
                                                </div>
                                                                        <div class="relatedtopics">
                                                                                <?php the_tags('',', ',''); ?>
                                                                        </div>
                                                <?php 
                                                }
                                                ?>
                                        
					</div>

					<div class="storycontent">
						<?php the_content(__('Read more')); ?>
                                                <?php wp_link_pages('before=<div class="pagelinks">&after=</div>&next_or_number=next&nextpagelink=Next&previouspagelink=Previous'); ?>
                                                <?php wp_link_pages('before=<div class="pagelinksnumbers">Page:&after=</div>'); ?>
                                        </div>
					<div class="postmeta">
						<ul class="articletools">
                                                      <li class="comments"><?php comments_popup_link(('Add a comment'), ('Comments (1)'), ('Comments (%)')); ?></li>
                                                      <li><?php if(function_exists('wp_print')) { print_link(); } ?></li>
                                                      <li><?php if(function_exists('wp_email')) { email_link(); } ?></li>
                                                      <li style="padding: 0px!important;"><a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="denverpost">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></li>
                                                      <li><iframe src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&amp;layout=button_count&amp;show_faces=false&amp;width=80&amp;action=like&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe></li>
                                                </ul>
                                                
                                                <div style="margin: 10px 0 0 155px;"><strong><a href="mailto:dpo@denverpost.com">Report an Error</a></strong>&nbsp;&nbsp;<strong>Categories:</strong> <?php the_category(', '); ?></div>
					</div>
                
                        <div class="postnav_single">
                                                <div class="right">
                                                                        <div class="postnavtitle_right">Next Post &raquo;</div>
                                                                        <div style="margin-right: 9px;"><?php next_post_link('%link'); ?></div>
                        </div>
                                                
                                                <div class="left">
                                                                        <div class="postnavtitle_left">&laquo; Previous Post</div>
                                                                        <div style="margin-left: 9px;"><?php previous_post_link('%link'); ?></div>
                                                </div>
                        </div>
                
			</div> <!-- Closes the post div-->
                        
                        <div id="respond"><div id="comments"><div class="relatedheader">Comments</div></div></div>
                        <div class="sidebarinfo" style="border-bottom: 3px solid #999; padding-bottom: 10px; margin-bottom: 10px;">
                                                <p>No registration or sign-in is required to leave a comment, but you may choose to sign in using Facebook, Twitter, Yahoo!, OpenID or DISQUS. Comments are moderated and may not appear immediately. See our <u><a href="http://neighbors.denverpost.com/viewtopic.php?f=56&t=123484135&start=0&st=0&sk=t&sd=a">commenting ground rules</a></u> for more information.</p>                                                
                        </div>
		<?php comments_template(); ?>
                
</div> <!-- Closes the content div-->

		<?php endwhile; else: ?>
		<?php include('404.php'); ?>
		<?php endif; ?>

		<a name="bottom"></a>

<?php include('footer.php'); ?>