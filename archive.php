<?php include('header.php'); ?>
	<?php include('sidebar2.php'); ?>
  
    <div id="content">
	    <div class="searchresult">Archives</div>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		  <div class="post" id="post-<?php the_ID(); ?>"></div>

<div class="post">
		<div class="thedate">Posted <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?></div>
				<div id="post-<?php the_ID(); ?>">
						<h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>

					<div class="theauthor">By <span class="postauthor"><?php coauthors_posts_links(); ?></span>			
					</div>

					<<div id="sidebar">
						<div class="feedback">
							<div class="sidebarcomments"><a href="<?php comments_link(); ?>"><?php comments_number('Add a comment','Comments (1)','Comments (%)'); ?></a></div>
							<div class="articletools"><?php if(function_exists('wp_print')) { print_link(); } ?></div>
							<div class="articletools"><?php if(function_exists('wp_email')) { email_link(); } ?></div>
                            <div class="articletools" style="margin-bottom: 8px!important;"><a name="fb_share" type="button_count" share_url="<?php the_permalink(); ?>" href="http://www.facebook.com/sharer.php">Share</a><script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script></div>
                            <div class="articletools"><iframe src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&amp;layout=button_count&amp;show_faces=false&amp;width=80&amp;action=like&amp;colorscheme=light&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe></div>
                            <div class="articletools">
								<g:plusone size="medium" href="<?php the_permalink(); ?>"></g:plusone>

								<script type="text/javascript">
  									(function() {
   									 var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    									po.src = 'https://apis.google.com/js/plusone.js';
    									var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
 									 })();
								</script></div>
                            <div class="articletools"><a href="http://twitter.com/share?url=<?php the_permalink(); ?>" class="twitter-share-button">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>
                        </div>
                                        
					</div>
                    
					<div class="storycontent">
						<?php the_content(__('Read more...')); ?>
					</div>
				</div><!-- Closes post-ID div -->
					<div class="postmeta">
                                             <div style="margin-left: 155px; margin-top: 10px;"><strong>Categories:</strong> <?php the_category(', '); ?></div>
					</div>
					<?php wp_link_pages(); ?> 
					<?php trackback_rdf(); ?>
	  </div> <!-- Closes the post div-->
      <?php endwhile; ?>
     <?php else : ?>
      Not Found
    <?php endif; ?>
  
  <div class="postnavigation">
				<div class="rightdouble">
					<?php previous_posts_link('Newer Entries &raquo;', '0') ?>
				</div>
				<div class="leftdouble">
                                        <?php next_posts_link('&laquo; Older Entries', 0); ?>
				</div>
       
			</div>
    
   </div> <!-- This closes the singlepost div-->
   
    <div id="bottomcontentdouble">
    </div>
<?php include('footer.php'); ?>